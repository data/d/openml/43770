# OpenML dataset: Xeno-Canto-Birds-from-India

https://www.openml.org/d/43770

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
https://www.kaggle.com/c/birdsong-recognition
Content
This is information captured about birds recording from India
Acknowledgements
We wouldn't be here without the help of many contributors to https://www.xeno-canto.org/: https://www.xeno-canto.org/contributors
Inspiration
You can use this data to start exploring the amazing landscape of birds recording. Based on frequency of birds recording in different areas, one can assess the species migration, species evolution as well as impact of various factors like air pollution (birds are an early indicator of pollution). Find interesting facts about the birds distribution and characteristics from the beautiful and rich in diversity Indian subcontinent.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43770) of an [OpenML dataset](https://www.openml.org/d/43770). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43770/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43770/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43770/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

